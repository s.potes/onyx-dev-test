import math

def CharCount(string,elem):
    if (  ((elem >= 65 ) and (elem <= 90) ) or ((elem >= 97 ) and (elem <= 122) )):
        i = 0
        k = 0 
        #acc = ord(elem) 
        acc = elem
        while(i < len(string)):
            if((string[i] == chr(elem)) or (string[i] == chr(acc + 32)) or (string[i] == chr(acc -32))):
                k += 1
            i += 1 
        return k
    else:
        return 0

def elemCount(string,elem):
    j = 0
    k = 0 
    temp = 0
    s = elem
    l = [ord(c) for c in s]
    a = set(l)
    l2 = list(a)
    while(j < len(l2)):
        k = CharCount(string,l2[j])
        temp += k
        #print(string,chr(l2[j]),k,temp)
        j += 1 
    
    return (temp/len(elem)) 

#palabra = "world"
#string = "hello world, BIG wORLD"


#palabra = "hello"
string = "hello world, BIG wORLD"

print("Times hello: ",elemCount(string,"hello"))

print("Times world: ",elemCount(string,"World"))

print("Times big: ",elemCount(string,"BIg"))

print("Times ,: ",elemCount(string,",")) 


#print("en la cadena",string ,"esa esta  ",CharCount(string,letra)," veces la ",letra)
